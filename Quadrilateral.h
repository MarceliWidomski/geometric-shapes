/*
 * Quadrilateral.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef QUADRILATERAL_H_
#define QUADRILATERAL_H_

#include "Figure.h"

class Quadrilateral: public Figure {
public:
	Quadrilateral(std::string name, double aSideLenght, double bSideLenght);
	void calculateAreaAndCircumfence();

	double getASideLenght() const {return this->aSideLenght;}
	void setASideLenght(double sideLenght);
	double getBSideLenght() const {return this->bSideLenght;}
	void setBSideLenght(double bSideLenght);
	void drawFigure() const;

protected:
	double aSideLenght;
	double bSideLenght;

};

#endif /* QUADRILATERAL_H_ */
