/*
 * EquilateralTriangle.cpp
 *
 *  Created on: 30.03.2017
 *      Author: marce
 */

#include "EquilateralTriangle.h"

EquilateralTriangle::EquilateralTriangle(std::string name, double sideLenght) :
		Triangle(name, sideLenght, sideLenght, sideLenght) {}
void EquilateralTriangle::specificShape() const{
	std::cout << "equilateral triangle" << std::endl;
	std::cout << "Lenght of side: " << this->aSideLenght << std::endl;
}


