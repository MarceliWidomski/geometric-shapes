///*
// * Figure.h
// *
// *  Created on: 29.03.2017
// *      Author: RENT
// */

#ifndef FIGURE_H_
#define FIGURE_H_

#include <iostream>
#include <string>
#include <cmath>


class Figure {
public:
	Figure(std::string name);
	void showName()const;
	void showArea()const;
	void showCircumference()const;
	virtual void showSpecification() const;

	double getArea() const {return area;}
	const std::string& getName() const {return name;}
	void setName(const std::string& name) {this->name = name;}
	double getCircumference() const {return circumference;}
	virtual void drawFigure() const =0;

protected:
	std::string name;
	double area;
	double circumference;
	virtual void specificShape() const = 0;
	virtual void calculateAreaAndCircumfence() = 0;

};
#endif /* FIGURE_H_ */
