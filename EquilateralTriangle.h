/*
 * EquilateralTriangle.h
 *
 *  Created on: 30.03.2017
 *      Author: marce
 */

#ifndef EQUILATERALTRIANGLE_H_
#define EQUILATERALTRIANGLE_H_

#include "Triangle.h"

class EquilateralTriangle: public Triangle {
public:
	EquilateralTriangle(std::string name, double sideLenght);
private:
void specificShape() const;
};
#endif /* EQUILATERALTRIANGLE_H_ */
