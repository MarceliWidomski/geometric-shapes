/*
 * Rectangle.cpp
 *
 *  Created on: 30.03.2017
 *      Author: marce
 */

#include "Rectangle.h"

Rectangle::Rectangle(std::string name, double aSideLenght, double bSideLenght) :
Quadrilateral(name, aSideLenght, bSideLenght) {}

void Rectangle::specificShape() const {
	std::cout << "rectangle" << std::endl;
	std::cout << "a side lenght: " << this->aSideLenght << std::endl;
	std::cout << "b side lenght: " << this->bSideLenght << std::endl;
}


