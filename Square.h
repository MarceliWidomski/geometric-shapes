/*
 * Square.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef SQUARE_H_
#define SQUARE_H_

#include "Rectangle.h"

class Square: public Rectangle {
public:
	Square(std::string name, double sideLenght);
protected:
	void specificShape() const;

};

#endif /* SQUARE_H_ */
