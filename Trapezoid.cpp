/*
 * Trapezoid.cpp
 *
 *  Created on: 30.03.2017
 *      Author: marce
 */

#include "Trapezoid.h"

Trapezoid::Trapezoid(std::string name, double altitude, double aSideLenght, double bSideLenght, double cSideLenght, double dSideLenght) :
	Quadrilateral(name, aSideLenght, bSideLenght){
	this->altitude=altitude;
	this -> cSideLenght=cSideLenght;
	this -> dSideLenght=dSideLenght;
	calculateAreaAndCircumfence();
}

void Trapezoid::calculateAreaAndCircumfence() {
		this->area = (this->aSideLenght + this->bSideLenght) * this->altitude
				/ 2;
	if (this->cSideLenght == 0 || this->dSideLenght == 0) {
		this->circumference = 0;
	} else
		this->circumference = this->aSideLenght + this->bSideLenght
				+ this->cSideLenght + this->dSideLenght;
}
void Trapezoid::specificShape() const {
	std::cout << "trapezoid" << std::endl;
	std::cout << "altitude lenght: " << this->altitude << std::endl;
	std::cout << "a side lenght: " << this->aSideLenght << std::endl;
	std::cout << "b side lenght: " << this->bSideLenght << std::endl;
	if (this->cSideLenght == 0)
		std::cout << "c side lenght: no data" << std::endl;
	else
		std::cout << "c side lenght: " << this->cSideLenght << std::endl;
	if (this->dSideLenght == 0)
		std::cout << "d side lenght: no data" << std::endl;
	else
		std::cout << "d side lenght: " << this->dSideLenght << std::endl;
}
void Trapezoid::drawFigure() const {
	std::cout<< "Drawing trapezoid!" << std::endl;
}
void Trapezoid::setAltitude(double altitude) {
	this->altitude = altitude;
	calculateAreaAndCircumfence();
}
void Trapezoid::setCSideLenght(double sideLenght) {
	cSideLenght = sideLenght;
	calculateAreaAndCircumfence();
}
void Trapezoid::setDSideLenght(double sideLenght) {
	dSideLenght = sideLenght;
	calculateAreaAndCircumfence();
}

