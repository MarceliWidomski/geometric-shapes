/*
 * Triangle.h
 *
 *  Created on: 30.03.2017
 *      Author: marce
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include "Figure.h"

class Triangle: public Figure {
public:
	Triangle(std::string name, double altitude, double aSideLenght);
	Triangle(std::string name, double aSideLenght, double bSideLenght, double cSideLenght);
	void calculateAreaAndCircumfence();
	void drawFigure() const;

	double getAltitude() const {return altitude;}
	double getASideLenght() const {return aSideLenght;}
	double getBSideLenght() const {return bSideLenght;}
	double getCSideLenght() const {return cSideLenght;}

protected:
	double altitude;
	double aSideLenght;
	double bSideLenght;
	double cSideLenght;
	void specificShape() const;
private:
	void calculateCircumfence();
};
#endif /* TRIANGLE_H_ */
