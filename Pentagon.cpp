/*
 * Pentagon.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "Pentagon.h"

Pentagon::Pentagon(std::string name, double sideLenght) :
Figure(name), sideLenght(sideLenght){
calculateAreaAndCircumfence();
}
void Pentagon::calculateAreaAndCircumfence(){
	this->area = (5/4.0) * (sideLenght*sideLenght) * (1/tan(36*M_PI/180));
	this->circumference = 5 * sideLenght;
}
void Pentagon::specificShape() const{
	std::cout << "pentagon" << std::endl;
	std::cout << "Side lenght: " << this->sideLenght << std::endl;
}
void Pentagon::drawFigure() const {
	std::cout<< "Drawing pentagon!" << std::endl;

}
void Pentagon::setSideLenght(double setSideLenght){
	this->sideLenght = sideLenght;
	calculateAreaAndCircumfence();
}
