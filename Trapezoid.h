/*
 * Trapezoid.h
 *
 *  Created on: 30.03.2017
 *      Author: marce
 */

#ifndef TRAPEZOID_H_
#define TRAPEZOID_H_

#include "Quadrilateral.h"

class Trapezoid: public Quadrilateral {
public:
	Trapezoid(std::string name, double altitude, double aSideLenght, double bSideLenght, double cSideLenght=0, double dSideLenght=0);
	void calculateAreaAndCircumfence();
	void drawFigure() const;

	double getAltitude() const {return altitude;}
	void setAltitude(double altitude);
	double getCSideLenght() const {return cSideLenght;}
	void setCSideLenght(double sideLenght);
	double getDSideLenght() const {return dSideLenght;}
	void setDSideLenght(double sideLenght);

private:
	void specificShape() const;
	double altitude;
	double cSideLenght;
	double dSideLenght;
};

#endif /* TRAPEZOID_H_ */
