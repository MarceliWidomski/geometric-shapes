/*
 * Circle.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "Circle.h"
#include <iostream>
#include <cmath>
using std::cout;
using std::endl;

Circle::Circle(std::string name, double radius) :
		Figure(name), radius(radius) {
	calculateAreaAndCircumfence();
}

void Circle::drawFigure() const {
std::cout << "Drawing circle!" << endl;
}
void Circle::calculateAreaAndCircumfence() {
	this->area = M_PI * this->radius * this->radius;
	this->circumference = 2 * M_PI * this->radius;
}
void Circle::setRadius(double radius) {
	this->radius = radius;
	calculateAreaAndCircumfence();
}
void Circle::specificShape() const{
	cout << "circle" << endl;
	cout << "Radius: " << this->radius << endl;
}

