/*
 * Circle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_

#include "Figure.h"

class Circle: public Figure {
public:
	Circle(std::string name, double radius);
	void drawFigure() const;
	void calculateAreaAndCircumfence();

	double getRadius() const {return radius;}
	void setRadius(double radius);

private:
	double radius;
	void specificShape() const;

};

#endif /* CIRCLE_H_ */
