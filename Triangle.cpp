/*
 * Triangle.cpp
 *
 *  Created on: 30.03.2017
 *      Author: marce
 */

#include "Triangle.h"

Triangle::Triangle(std::string name, double altitude, double aSideLenght) :
		Figure(name), altitude(altitude), aSideLenght(aSideLenght) {
	this->bSideLenght=0;
	this->cSideLenght=0;
	calculateAreaAndCircumfence();
}
Triangle::Triangle(std::string name, double aSideLenght, double bSideLenght, double cSideLenght) :
	Figure (name), aSideLenght(aSideLenght), bSideLenght(bSideLenght), cSideLenght(cSideLenght) {
	this->altitude=0;
	calculateAreaAndCircumfence();
}
void Triangle::calculateAreaAndCircumfence() {
	if (this->altitude == 0) {
		double p = (this->aSideLenght + this->bSideLenght + this->cSideLenght) / 2;
		this->area = sqrt(
				p * (p - aSideLenght) * (p - bSideLenght) * (p - cSideLenght));
	} else
		this->area = this->aSideLenght * this->altitude / 2;
	calculateCircumfence();
}
void Triangle::calculateCircumfence(){
	if (bSideLenght==0 || cSideLenght==0)
		this->circumference=0;
	else
	this->circumference = this->aSideLenght + this->bSideLenght + this-> cSideLenght;
}
void Triangle::drawFigure() const{
	std::cout << "Drawing triangle!" << std::endl;
}
void Triangle::specificShape() const{
	std::cout << "triangle" << std::endl;
	if (this->altitude==0){
		std::cout << "Lenght of a side: " << this->aSideLenght << std::endl;
		std::cout << "Lenght of b side: " << this->bSideLenght << std::endl;
		std::cout << "Lenght of c side: " << this->cSideLenght << std::endl;
	}
	else{
	std::cout << "Lenght of side: " << this->aSideLenght << std::endl;
	std::cout << "Lenght of altitude: " << this->altitude << std::endl;
	}
}

