///*
// * Figure.cpp
// *
// *  Created on: 29.03.2017
// *      Author: RENT
// */


#include "Figure.h"

Figure::Figure(std::string name):name(name){

}
void Figure::showName()const{
	std::cout << "Name: " << this->name << std::endl;
}
void Figure::showArea()const{
	std::cout << "Area of " << name << ": "<< getArea() << std::endl;
}
void Figure::showCircumference()const{
	std::cout << "Circumference of " << name << ": ";
	if (this->circumference==0)
		std::cout << "no data" << std::endl;
	else
	std::cout << getCircumference() << std::endl;
}
void Figure::showSpecification() const {
	std::cout << "Specification of " << this->name << ":" << std::endl;
	std::cout << "Figure type: ";
	specificShape();
	showCircumference();
	showArea();
	std::cout << std::endl;
}
