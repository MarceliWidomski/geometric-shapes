/*
 * Rectangle.h
 *
 *  Created on: 30.03.2017
 *      Author: marce
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_

#include "Quadrilateral.h"

class Rectangle: public Quadrilateral {
public:
	Rectangle(std::string name, double aSideLenght, double bSideLenght);
protected:
	void specificShape() const;

};

#endif /* RECTANGLE_H_ */
