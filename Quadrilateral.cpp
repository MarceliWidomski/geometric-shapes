/*
 * Quadrilateral.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "Quadrilateral.h"

Quadrilateral::Quadrilateral(std::string name, double aSideLenght,double bSideLenght) :
		Figure(name), aSideLenght(aSideLenght), bSideLenght(bSideLenght) {
	calculateAreaAndCircumfence();

}
void Quadrilateral::calculateAreaAndCircumfence() {
	this->area = this->aSideLenght * this->bSideLenght;
	this->circumference = 2 * this->aSideLenght + 2 * this->bSideLenght;
}
void Quadrilateral::setBSideLenght(double bSideLenght) {
	this->bSideLenght = bSideLenght;
	calculateAreaAndCircumfence();
}
void Quadrilateral::setASideLenght(double aSideLenght) {
	this->aSideLenght = aSideLenght;
	this->bSideLenght=aSideLenght;
	calculateAreaAndCircumfence();
}
void Quadrilateral::drawFigure() const {
	std::cout <<"Draw:" << std::endl;
	for (int i = 0; i < aSideLenght; ++i) {
		for (int j = 0; j < bSideLenght; ++j)
			std::cout << "=";
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

