/*
 * IsoscelesTriangle.h
 *
 *  Created on: 30.03.2017
 *      Author: marce
 */

#ifndef ISOSCELESTRIANGLE_H_
#define ISOSCELESTRIANGLE_H_

#include "Triangle.h"

class IsoscelesTriangle: public Triangle {
public:
	IsoscelesTriangle(std::string name, double aSideLenght, double bSideLenght);
private:
	void specificShape() const;

};

#endif /* ISOSCELESTRIANGLE_H_ */
