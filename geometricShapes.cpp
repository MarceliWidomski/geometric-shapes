//============================================================================
// Name        : geometricShapes.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Figure.h"
#include "Circle.h"
#include "Quadrilateral.h"
#include "Square.h"
#include "Trapezoid.h"
#include "EquilateralTriangle.h"
#include "IsoscelesTriangle.h"
#include "Pentagon.h"

using namespace std;

int main() {

	Circle A("A", 5);
	Square B("B", 5);
	Rectangle C("C", 5, 6);
	Trapezoid D("D", 4, 6, 3);
	Triangle E("E", 3, 4);
	EquilateralTriangle F("F", 5);
	IsoscelesTriangle G("G", 4, 6);
	Pentagon H ("H", 6);

	int size(8);
	Figure* figures[size]={&A, &B, &C, &D, &E, &F, &G, &H}; //array of pointers at objects of Figure class and derivered classes
	for (int i=0; i<size;++i){
		figures[i]->showName();
		figures[i]->showArea();
		figures[i]->showCircumference();
		figures[i]->showSpecification();
		figures[i]->drawFigure();
	}

	return 0;
}
