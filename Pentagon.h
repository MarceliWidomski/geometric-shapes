/*
 * Pentagon.h
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef PENTAGON_H_
#define PENTAGON_H_

#include "Figure.h"

class Pentagon: public Figure {
public:
	Pentagon(std::string name, double sideLenght);
	void calculateAreaAndCircumfence();
	void drawFigure() const;

	double getSideLenght() const {return sideLenght;}
	void setSideLenght(double setSideLenght);
private:
	double sideLenght;
	void specificShape() const;

};

#endif /* PENTAGON_H_ */
