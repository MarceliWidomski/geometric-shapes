/*
 * Square.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "Square.h"
using std::cout;
using std::endl;

Square::Square(std::string name, double sideLenght): Rectangle(name, sideLenght, sideLenght) {}

void Square::specificShape() const {
	cout << "square" << endl;
	cout << "Side lenght: " << this->aSideLenght << endl;
}
